/*
 *
 * libcoreaws4/src/coreaws4/HttpUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__HTTP_UTILS
#define COREAWS4__HTTP_UTILS

#include <string>

#include "ParameterEntry.hpp"
#include "Request.hpp"

namespace coreaws4
{

class HttpUtils
{
public:
    HttpUtils() = delete;
    HttpUtils(const HttpUtils&) = delete;
    HttpUtils(HttpUtils&&) = delete;
    ~HttpUtils() = delete;
    HttpUtils& operator=(const HttpUtils&) = delete;
    HttpUtils& operator=(HttpUtils&&) = delete;

    static std::string encodeParameters(const Request& request);
    static std::string urlEncode(const std::string& str);
    static std::string urlEncodePath(const std::string& path);
private:
    static std::string urlEncodeCharacter(const std::string& acc, char c);
    static std::string urlEncodePathCharacter(const std::string& acc, char c);
    static std::string escapeCharacter(char c);
    static bool isNormalCharacter(char c);
    static bool isNormalPathCharacter(char c);
    static std::string checkQueryString(const std::string& queryString);
    static std::string buildQueryString(const std::string& acc,
                                        const ParameterEntry& parameter);
};

}

#endif // not COREAWS4__HTTP_UTILS

