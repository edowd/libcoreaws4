/*
 *
 * libcoreaws4/src/coreaws4/AWS3Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>

#include <boost/optional.hpp>

#include "BinaryUtils.hpp"
#include "Buffer.hpp"
#include "DateTimeUtils.hpp"
#include "HashUtils.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws4
{

AWS3Signer::~AWS3Signer()
{
}

Request AWS3Signer::sign(const Credentials& credentials,
                         const Request& request,
                         std::time_t time)
{
    HeaderMap headers(Signer::filterXAmznAuthorization(request.getHeaders()));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    auto sessionToken = credentials.getSessionToken();
    if (sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        boost::optional<std::string> value = sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("host") == lowerHeaders.end())
    {
        std::string key = "Host";
        boost::optional<std::string> value =
                boost::optional<std::string>(request.getEndpoint().getServer());
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string rfc822 = DateTimeUtils::formatRFC822(time);

    if (lowerHeaders.find("date") == lowerHeaders.end())
    {
        std::string key = "Date";
        boost::optional<std::string> value = boost::optional<std::string>(rfc822);
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "X-Amz-Date";
        boost::optional<std::string> value = boost::optional<std::string>(rfc822);
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = AWS3Signer::stringToSign(Request(request).
            withHeaders(headers));

    std::string algorithm = "HmacSHA256";
    std::string digest = "sha256";

    Buffer bufferToSign =
            HashUtils::hash(digest, BinaryUtils::fromString(stringToSign));
    std::string signature = Signer::sign(algorithm,
                                         credentials.getSecretAccessKey(),
                                         bufferToSign);

    std::stringstream authHeader;
    authHeader << "AWS3 AWSAccessKeyId=" << credentials.getAccessKeyId()
               << ",Algorithm=" << algorithm
               << ",SignedHeaders=" << AWS3Signer::signedHeaders(headers)
               << ",Signature=" << signature;

    headers.insert(HeaderEntry("X-Amzn-Authorization",
                               boost::optional<std::string>(authHeader.str())));

    return Request(request).
            withHeaders(headers);
}

Request AWS3Signer::operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const
{
    return AWS3Signer::sign(credentials, request, time);
}

std::string AWS3Signer::stringToSign(const Request& request)
{
    InputStreamHandle content = (request.getContent()) ?
            *(request.getContent()) : StreamUtils::emptyInputStream();

    HeaderMap lowerHeaders(Signer::lowerCaseKey(request.getHeaders()));
    std::stringstream strbuf;

    strbuf << show(request.getHttpMethod()) << '\n';
    strbuf << Signer::canonicalizedResourcePath(request.getResourcePath()) << '\n';
    strbuf << Signer::canonicalizedQueryString(request.getParameters()) << '\n';
    strbuf << AWS3Signer::canonicalHeaders(lowerHeaders) << '\n';
    strbuf << BinaryUtils::toString(StreamUtils::getInputContents(content));

    return strbuf.str();
}

std::string AWS3Signer::canonicalHeaders(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders;
    std::copy_if(
            lowerHeaders.begin(),
            lowerHeaders.end(),
            std::inserter(interestingHeaders,
                          interestingHeaders.end()),
            [] (const HeaderEntry& header) {
                return (header.first == "host" ||
                        StringUtils::startsWith(header.first, "x-amz"));
            });

    return std::accumulate(
            interestingHeaders.begin(),
            interestingHeaders.end(),
            std::string(),
            [] (const std::string& acc, const HeaderEntry& header) {
                std::stringstream strbuf;
                strbuf << acc;
                strbuf << header.first;
                strbuf << ':';
                if (header.second)
                {
                    strbuf << *(header.second);
                }
                strbuf << '\n';
                return strbuf.str();
            });
}

std::string AWS3Signer::signedHeaders(const HeaderMap& headers)
{
    return std::accumulate(
            headers.begin(),
            headers.end(),
            std::string("Host"),
            [] (const std::string& acc, const HeaderEntry& header) {
                std::stringstream strbuf;
                strbuf << acc;
                if (StringUtils::startsWith(StringUtils::toLowerCase(header.first), "x-amz"))
                {
                    strbuf << ';' << header.first;
                }

                return strbuf.str();
            });
}

}

