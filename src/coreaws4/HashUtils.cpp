/*
 *
 * libcoreaws4/src/coreaws4/HashUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HashUtils.hpp"

#include <sstream>
#include <stdexcept>

#include <openssl/evp.h>
#include <openssl/hmac.h>

#include "BinaryUtils.hpp"

namespace coreaws4
{

Buffer HashUtils::hash(const std::string& algorithm, const Buffer& data)
{
    OpenSSL_add_all_digests();

    const EVP_MD* md = EVP_get_digestbyname(algorithm.c_str());
    if (!md)
    {
        std::stringstream errbuf;
        errbuf << "Unknown message digest: \"" << algorithm << '"';
        throw std::invalid_argument(errbuf.str());
    }

    EVP_MD_CTX ctx;
    unsigned char* buf = new unsigned char[EVP_MAX_MD_SIZE];
    unsigned int len;

    EVP_MD_CTX_init(&ctx);
    EVP_DigestInit_ex(&ctx, md, NULL);
    EVP_DigestUpdate(&ctx, data.data(), data.size());
    EVP_DigestFinal_ex(&ctx, buf, &len);

    Buffer buffer = BinaryUtils::fromArray(buf, len);

    delete [] buf;
    EVP_MD_CTX_cleanup(&ctx);

    return buffer;
}

Buffer HashUtils::hmacHash(const std::string& algorithm,
                           const Buffer& key,
                           const Buffer& data)
{
    OpenSSL_add_all_digests();

    const EVP_MD* md = EVP_get_digestbyname(algorithm.c_str());
    if (!md)
    {
        std::stringstream errbuf;
        errbuf << "Unknown message digest: \"" << algorithm << '"';
        throw std::invalid_argument(errbuf.str());
    }

    HMAC_CTX ctx;
    HMAC_CTX_init(&ctx);
    HMAC_Init(&ctx, key.data(), key.size(), md);

    unsigned char* buf = new unsigned char[EVP_MAX_MD_SIZE];
    unsigned int len;
    HMAC(md, key.data(), key.size(), data.data(), data.size(), buf, &len);

    Buffer buffer = BinaryUtils::fromArray(buf, len);

    delete [] buf;
    HMAC_CTX_cleanup(&ctx);

    return buffer;
}

}

