/*
 *
 * libcoreaws4/src/coreaws4/S3Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__S3_SIGNER
#define COREAWS4__S3_SIGNER

#include "Signer.hpp"

namespace coreaws4
{

class S3Signer : public Signer
{
public:
    S3Signer() = default;
    S3Signer(const S3Signer&) = delete;
    S3Signer(S3Signer&&) = delete;
    virtual ~S3Signer();
    S3Signer& operator=(const S3Signer&) = delete;
    S3Signer& operator=(Signer&&) = delete;

    static Request sign(const Credentials& credentials,
                        const Request& request,
                        std::time_t time);

    virtual Request operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const;

protected:
    static std::string stringToSign(const Request& request);
};

}

#endif // not COREAWS4__S3_SIGNER

