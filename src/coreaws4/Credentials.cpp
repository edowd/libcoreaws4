/*
 *
 * libcoreaws4/src/coreaws4/Credentials.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Credentials.hpp"

#include <utility>

namespace coreaws4
{

Credentials::Credentials()
{
}

Credentials::Credentials(const Credentials& copy)
{
    accessKeyId = copy.accessKeyId;
    secretAccessKey = copy.secretAccessKey;
    sessionToken = copy.sessionToken;
}

Credentials::Credentials(Credentials&& move)
{
    accessKeyId = std::move(move.accessKeyId);
    secretAccessKey = std::move(move.secretAccessKey);
    sessionToken = std::move(move.sessionToken);
}

Credentials::~Credentials()
{
}

Credentials& Credentials::operator=(const Credentials& rhs)
{
    if (this == &rhs)
    {
        return *this;
    }

    accessKeyId = rhs.accessKeyId;
    secretAccessKey = rhs.secretAccessKey;
    sessionToken = rhs.sessionToken;

    return *this;
}

Credentials& Credentials::operator=(Credentials&& rhs)
{
    accessKeyId = std::move(rhs.accessKeyId);
    secretAccessKey = std::move(rhs.secretAccessKey);
    sessionToken = std::move(rhs.sessionToken);

    return *this;
}

Credentials::Credentials(const std::string& accessKeyId,
                         const std::string& secretAccessKey) :
        accessKeyId(accessKeyId),
        secretAccessKey(secretAccessKey),
        sessionToken(boost::optional<std::string>())
{
}

Credentials::Credentials(const std::string& accessKeyId,
                         const std::string& secretAccessKey,
                         const std::string& sessionToken) :
        accessKeyId(accessKeyId),
        secretAccessKey(secretAccessKey),
        sessionToken(boost::optional<std::string>(sessionToken))
{
}

Credentials::Credentials(const std::string& accessKeyId,
                         const std::string& secretAccessKey,
                         const boost::optional<std::string>& sessionToken) :
        accessKeyId(accessKeyId),
        secretAccessKey(secretAccessKey),
        sessionToken(sessionToken)
{
}

bool Credentials::operator==(const Credentials& rhs) const
{
    return (accessKeyId == rhs.accessKeyId &&
            secretAccessKey == rhs.secretAccessKey &&
            sessionToken == rhs.sessionToken);
}

bool Credentials::operator!=(const Credentials& rhs) const
{
    return !((*this) == rhs);
}

std::string Credentials::getAccessKeyId() const
{
    return accessKeyId;
}

void Credentials::setAccessKeyId(const std::string& accessKeyId)
{
    this->accessKeyId = accessKeyId;
}

Credentials& Credentials::withAccessKeyId(const std::string& accessKeyId)
{
    setAccessKeyId(accessKeyId);
    return *this;
}

std::string Credentials::getSecretAccessKey() const
{
    return secretAccessKey;
}

void Credentials::setSecretAccessKey(const std::string& secretAccessKey)
{
    this->secretAccessKey = secretAccessKey;
}

Credentials& Credentials::withSecretAccessKey(const std::string& secretAccessKey)
{
    setSecretAccessKey(secretAccessKey);
    return *this;
}

boost::optional<std::string> Credentials::getSessionToken() const
{
    return sessionToken;
}

void Credentials::setSessionToken(const boost::optional<std::string>& sessionToken)
{
    this->sessionToken = sessionToken;
}

void Credentials::setSessionToken(const std::string& sessionToken)
{
    this->sessionToken = boost::optional<std::string>(sessionToken);
}

Credentials& Credentials::withSessionToken(const boost::optional<std::string>& sessionToken)
{
    setSessionToken(sessionToken);
    return *this;
}

Credentials& Credentials::withSessionToken(const std::string& sessionToken)
{
    setSessionToken(sessionToken);
    return *this;
}

}

