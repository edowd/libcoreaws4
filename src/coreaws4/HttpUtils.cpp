/*
 *
 * libcoreaws4/src/coreaws4/HttpUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HttpUtils.hpp"

#include <cctype>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <sstream>

namespace coreaws4
{

std::string HttpUtils::encodeParameters(const Request& request)
{
    auto parameters = request.getParameters();
    return HttpUtils::checkQueryString(
            std::accumulate(parameters.begin(),
                            parameters.end(),
                            std::string(),
                            HttpUtils::buildQueryString));
}

std::string HttpUtils::urlEncode(const std::string& str)
{
    return std::accumulate(str.begin(),
                           str.end(),
                           std::string(),
                           HttpUtils::urlEncodeCharacter);
}

std::string HttpUtils::urlEncodePath(const std::string& str)
{
    return std::accumulate(str.begin(),
                           str.end(),
                           std::string(),
                           HttpUtils::urlEncodePathCharacter);
}

std::string HttpUtils::urlEncodeCharacter(const std::string& acc, char c)
{
    if (HttpUtils::isNormalCharacter(c))
    {
        return acc + c;
    }
    else
    {
        return acc + escapeCharacter(c);
    }
}

std::string HttpUtils::urlEncodePathCharacter(const std::string& acc, char c)
{
    if (HttpUtils::isNormalPathCharacter(c))
    {
        return acc + c;
    }
    else
    {
        return acc + escapeCharacter(c);
    }
}

std::string HttpUtils::escapeCharacter(char c)
{
    char buf[4];
    std::memset(buf, 0, 4);
    buf[0] = '%';
    std::sprintf(buf + 1, "%02X", c);
    return std::string(buf);
}

bool HttpUtils::isNormalCharacter(char c)
{
    return (std::isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~');
}

bool HttpUtils::isNormalPathCharacter(char c)
{
    return (c == '/' || isNormalCharacter(c));
}

std::string HttpUtils::checkQueryString(const std::string& queryString)
{
    if (!queryString.empty())
    {
        return queryString.substr(1);
    }
    else
    {
        return queryString;
    }
}

std::string HttpUtils::buildQueryString(const std::string& acc,
                                        const ParameterEntry& parameter)
{
    std::stringstream strbuf;
    strbuf << '&';
    strbuf << HttpUtils::urlEncode(parameter.first);
    if (parameter.second)
    {
        strbuf << '=' << HttpUtils::urlEncode(*(parameter.second));
    }
    return acc + strbuf.str();
}

}

