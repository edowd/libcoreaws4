/*
 *
 * libcoreaws4/src/coreaws4/ParameterEntry.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__PARAMETER_ENTRY
#define COREAWS4__PARAMETER_ENTRY

#include <string>
#include <utility>

#include <boost/optional.hpp>

namespace coreaws4
{

typedef std::pair<const std::string, boost::optional<std::string> > ParameterEntry;

}

#endif // not COREAWS4__PARAMETER_ENTRY

