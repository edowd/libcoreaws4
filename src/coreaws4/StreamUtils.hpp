/*
 *
 * libcoreaws4/src/coreaws4/StreamUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__STREAM_UTILS
#define COREAWS4__STREAM_UTILS

#include <cstdlib>
#include <string>

#include "Buffer.hpp"
#include "InputStreamHandle.hpp"
#include "OutputStreamHandle.hpp"

namespace coreaws4
{

class StreamUtils
{
public:
    StreamUtils() = delete;
    StreamUtils(const StreamUtils&) = delete;
    StreamUtils(StreamUtils&&) = delete;
    ~StreamUtils() = delete;
    StreamUtils& operator=(const StreamUtils&) = delete;
    StreamUtils& operator=(StreamUtils&&) = delete;

    static InputStreamHandle emptyInputStream();
    static OutputStreamHandle emptyOutputStream();
    static Buffer getInputContents(InputStreamHandle inputStream);
    static std::size_t getInputSize(InputStreamHandle inputStream);
    static InputStreamHandle openInputFile(const std::string& filename);
    static OutputStreamHandle openOutputFile(const std::string& filename);
    static InputStreamHandle setInput(const Buffer& input);
    static InputStreamHandle setInput(const std::string& input);
};

}

#endif // not COREAWS4__STREAM_UTILS

