/*
 *
 * libcoreaws4/src/coreaws4/ChainCredentialsProvider.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "ChainCredentialsProvider.hpp"

#include <cstdlib>
#include <sstream>
#include <stdexcept>

#include <numeric>

#include "Credentials.hpp"

namespace coreaws4
{

ChainCredentialsProvider::~ChainCredentialsProvider()
{
}

ChainCredentialsProvider::ChainCredentialsProvider(
        const std::list<CredentialsProviderHandle> credentialsProviders) :
        credentialsProviders(credentialsProviders)
{
}

boost::optional<Credentials> ChainCredentialsProvider::operator()()
{
    return this->refresh();
}

boost::optional<Credentials> ChainCredentialsProvider::refresh()
{
    return std::accumulate(
            credentialsProviders.begin(),
            credentialsProviders.end(),
            boost::optional<Credentials>(),
            [] (const boost::optional<Credentials>& acc,
                    CredentialsProviderHandle provider) {
                if (acc || !provider)
                {
                    return acc;
                }

                return (*provider)();
            });
}

}

