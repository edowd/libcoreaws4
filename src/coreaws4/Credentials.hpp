/*
 *
 * libcoreaws4/src/coreaws4/Credentials.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__CREDENTIALS
#define COREAWS4__CREDENTIALS

#include <string>

#include <boost/optional.hpp>

namespace coreaws4
{

class Credentials
{
public:
    Credentials();
    Credentials(const Credentials& copy);
    Credentials(Credentials&& move);
    ~Credentials();
    Credentials& operator=(const Credentials& rhs);
    Credentials& operator=(Credentials&& rhs);

    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey,
                const boost::optional<std::string>& sessionToken);
    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey);
    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey,
                const std::string& sessionToken);

    bool operator==(const Credentials& rhs) const;
    bool operator!=(const Credentials& rhs) const;

    std::string getAccessKeyId() const;
    void setAccessKeyId(const std::string& accessKeyId);
    Credentials& withAccessKeyId(const std::string& accessKeyId);

    std::string getSecretAccessKey() const;
    void setSecretAccessKey(const std::string& secretAccessKey);
    Credentials& withSecretAccessKey(const std::string& secretAccessKey);

    boost::optional<std::string> getSessionToken() const;
    void setSessionToken(const boost::optional<std::string>& sessionToken);
    void setSessionToken(const std::string& sessionToken);
    Credentials& withSessionToken(const boost::optional<std::string>& sessionToken);
    Credentials& withSessionToken(const std::string& sessionToken);

private:
    std::string accessKeyId;
    std::string secretAccessKey;
    boost::optional<std::string> sessionToken;
};

}

#endif // not COREAWS4__CREDENTIALS

