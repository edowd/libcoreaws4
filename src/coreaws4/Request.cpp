/*
 *
 * libcoreaws4/src/coreaws4/Request.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Request.hpp"

#include <utility>

#include <iostream>

namespace coreaws4
{

Request::Request()
{
}

Request::Request(const Request& copy)
{
    httpMethod = copy.httpMethod;
    endpoint = copy.endpoint;
    resourcePath = copy.resourcePath;
    parameters = copy.parameters;
    headers = copy.headers;
    content = copy.content;
}

Request::Request(Request&& move)
{
    httpMethod = std::move(move.httpMethod);
    endpoint = std::move(move.endpoint);
    resourcePath = std::move(move.resourcePath);
    parameters = std::move(move.parameters);
    headers = std::move(move.headers);
    content = std::move(move.content);
}

Request::~Request()
{
}

Request& Request::operator=(const Request& rhs)
{
    if (this == &rhs)
    {
        return *this;
    }

    httpMethod = rhs.getHttpMethod();
    endpoint = rhs.getEndpoint();
    resourcePath = rhs.getResourcePath();
    parameters = rhs.getParameters();
    headers = rhs.getHeaders();
    content = rhs.getContent();

    return *this;
}

Request& Request::operator=(Request&& rhs)
{
    httpMethod = std::move(rhs.httpMethod);
    endpoint = std::move(rhs.endpoint);
    resourcePath = std::move(rhs.resourcePath);
    parameters = std::move(rhs.parameters);
    headers = std::move(rhs.headers);
    content = std::move(rhs.content);

    return *this;
}

Request::Request(const HttpMethod& httpMethod,
                 const Endpoint& endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers,
                 const boost::optional<InputStreamHandle>& content) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(content)
{
}

Request::Request(const HttpMethod& httpMethod,
                 const Endpoint& endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers,
                 InputStreamHandle content) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(boost::optional<InputStreamHandle>(content))
{
}

Request::Request(const HttpMethod& httpMethod,
                 const Endpoint& endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(boost::optional<InputStreamHandle>())
{
}

bool Request::operator==(const Request& rhs) const
{
    return (httpMethod == rhs.httpMethod &&
            endpoint == rhs.endpoint &&
            resourcePath == rhs.resourcePath &&
            parameters == rhs.parameters &&
            headers == rhs.headers &&
            content == rhs.content);
}

bool Request::operator!=(const Request& rhs) const
{
    return !((*this) == rhs);
}

HttpMethod Request::getHttpMethod() const
{
    return httpMethod;
}

void Request::setHttpMethod(const HttpMethod& httpMethod)
{
    this->httpMethod = httpMethod;
}

Request& Request::withHttpMethod(const HttpMethod& httpMethod)
{
    setHttpMethod(httpMethod);
    return *this;
}

Endpoint Request::getEndpoint() const
{
    return endpoint;
}

void Request::setEndpoint(const Endpoint& endpoint)
{
    this->endpoint = endpoint;
}

Request& Request::withEndpoint(const Endpoint& endpoint)
{
    setEndpoint(endpoint);
    return *this;
}

std::string Request::getResourcePath() const
{
    return resourcePath;
}

void Request::setResourcePath(const std::string& resourcePath)
{
    this->resourcePath = resourcePath;
}

Request& Request::withResourcePath(const std::string& resourcePath)
{
    setResourcePath(resourcePath);
    return *this;
}

ParameterMap Request::getParameters() const
{
    return parameters;
}

void Request::setParameters(const ParameterMap& parameters)
{
    this->parameters = parameters;
}

Request& Request::withParameters(const ParameterMap& parameters)
{
    setParameters(parameters);
    return *this;
}

HeaderMap Request::getHeaders() const
{
    return headers;
}

void Request::setHeaders(const HeaderMap& headers)
{
    this->headers = headers;
}

Request& Request::withHeaders(const HeaderMap& headers)
{
    setHeaders(headers);
    return *this;
}

boost::optional<InputStreamHandle> Request::getContent() const
{
    return content;
}

void Request::setContent(const boost::optional<InputStreamHandle>& content)
{
    this->content = content;
}

void Request::setContent(InputStreamHandle content)
{
    this->content = boost::optional<InputStreamHandle>(content);
}

Request& Request::withContent(const boost::optional<InputStreamHandle>& content)
{
    setContent(content);
    return *this;
}

Request& Request::withContent(InputStreamHandle content)
{
    setContent(content);
    return *this;
}

}

