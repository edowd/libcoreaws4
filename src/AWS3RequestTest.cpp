/*
 *
 * libcoreaws4/src/AWS3RequestTest.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <sstream>
#include <string>

#include "common.hpp"

int main(int argc, char** argv)
{
    try
    {
    coreaws4::Connection::init();

    coreaws4::SignerHandle signer(new coreaws4::AWS3Signer());
    coreaws4::CredentialsProviderHandle credentialsProvider(
            new coreaws4::EnvironmentVariableCredentialsProvider());

    coreaws4::HttpMethod method = coreaws4::HttpMethod::POST;
    coreaws4::Endpoint endpoint(
            coreaws4::Scheme::https,
            "email.us-east-1.amazonaws.com",
            boost::optional<coreaws4::Port>());
    std::string resourcePath = "/";
    coreaws4::ParameterMap parameters;
    parameters.insert(coreaws4::ParameterEntry(
            "Version", 
            boost::optional<std::string>("2010-12-01")));
    parameters.insert(coreaws4::ParameterEntry(
            "Action",
            boost::optional<std::string>("VerifyEmailAddress")));
    parameters.insert(coreaws4::ParameterEntry(
            "EmailAddress",
            boost::optional<std::string>("user@example.com")));
    coreaws4::HeaderMap headers;
    boost::optional<coreaws4::InputStreamHandle> content =
            boost::optional<coreaws4::InputStreamHandle>();

    auto request = coreaws4::Request(
            method,
            endpoint,
            resourcePath,
            parameters,
            headers,
            content);

    auto signedRequest =
            (*signer)(credentialsProvider,
                      request,
                      coreaws4::DateTimeUtils::now());

    coreaws4::ResponseHandle response =
            coreaws4::Connection::execute(signedRequest);

    if (CURLE_OK != response->returnCode)
    {
        std::cerr << "Error: " << response->errorMessage << " ["
                  << curl_easy_strerror(response->returnCode) << "] ("
                  << response->returnCode << ')' << std::endl;
        return 1;
    }

    coreaws4::OutputStreamHandle outputStream = response->output;
    std::shared_ptr<std::stringstream> output =
            std::static_pointer_cast<std::stringstream>(outputStream);
    std::cout << output->str() << std::endl;

    coreaws4::Connection::cleanup();

    return 0;
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
        return 1;
    }
}

