/*
 *
 * libcoreaws4/src/S3RequestTest.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <sstream>
#include <string>

#include <curl/curl.h>

#include "common.hpp"

const char* BUCKET = "dowdandassociates-test";
const char* KEY = "test x.txt";

int main(int argc, char** argv)
{
    try
    {
        coreaws4::Connection::init();

        coreaws4::SignerHandle signer(new coreaws4::S3Signer());
        coreaws4::CredentialsProviderHandle credentialsProvider(
                new coreaws4::EnvironmentVariableCredentialsProvider());

        coreaws4::HttpMethod putMethod = coreaws4::HttpMethod::PUT;
        coreaws4::Endpoint endpoint(
                coreaws4::Scheme::https,
                "s3.amazonaws.com",
                boost::optional<coreaws4::Port>());
        std::stringstream strbuf;
        strbuf << '/' << BUCKET << '/' << KEY;
        std::string resourcePath = strbuf.str();
        coreaws4::ParameterMap parameters;
        coreaws4::HeaderMap headers;
        boost::optional<coreaws4::InputStreamHandle> putContent =
                boost::optional<coreaws4::InputStreamHandle>(
                        coreaws4::StreamUtils::openInputFile("input.txt"));

        auto putRequest = coreaws4::Request(
                putMethod,
                endpoint,
                resourcePath,
                parameters,
                headers,
                putContent);


        auto signedPutRequest =
                (*signer)(credentialsProvider,
                          putRequest,
                          coreaws4::DateTimeUtils::now());


        coreaws4::ResponseHandle putResponse =
                coreaws4::Connection::execute(signedPutRequest);

        if (CURLE_OK != putResponse->returnCode)
        {
            std::cerr << "Error: " << putResponse->errorMessage << " ["
                      << curl_easy_strerror(putResponse->returnCode) << "] ("
                      << putResponse->returnCode << ')' << std::endl;
            return 1;
        }

        coreaws4::OutputStreamHandle putOutputStream = putResponse->output;
        std::shared_ptr<std::stringstream> putOutput =
                std::static_pointer_cast<std::stringstream>(
                        putOutputStream);
        std::cout << putOutput->str() << std::endl;

        coreaws4::HttpMethod getMethod = coreaws4::HttpMethod::GET;
    
        boost::optional<coreaws4::InputStreamHandle> getContent =
                boost::optional<coreaws4::InputStreamHandle>();

        auto getRequest = coreaws4::Request(
                getMethod,
                endpoint,
                resourcePath,
                parameters,
                headers,
                getContent);

        coreaws4::OutputStreamHandle outputFile =
                coreaws4::StreamUtils::openOutputFile("output.txt");

        auto signedGetRequest =
                (*signer)(credentialsProvider,
                          getRequest,
                          coreaws4::DateTimeUtils::now());

        coreaws4::ResponseHandle getResponse =
                coreaws4::Connection::execute(signedGetRequest, outputFile);


        if (CURLE_OK != getResponse->returnCode)
        {
            std::cerr << "Error: " << getResponse->errorMessage << " ["
                      << curl_easy_strerror(getResponse->returnCode) << "] ("
                      << getResponse->returnCode << ')' << std::endl;
            return 1;
        }

        coreaws4::Connection::cleanup();
        return 0;
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
        return 1;
    }

}

