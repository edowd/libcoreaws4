/*
 *
 * libcoreaws4/src/common.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "common.hpp"

#include <cstdlib>
#include <fstream>
#include <istream>
#include <sstream>
#include <stdexcept>

#include <iostream>

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        const coreaws4::Credentials& credentials,
        const coreaws4::Request& request,
        std::time_t time)
{
    return requestToCurl((*signer)(credentials, request, time));
}


std::string requestToCurl(
        coreaws4::SignerHandle signer,
        const coreaws4::Credentials& credentials,
        const coreaws4::Request& request)
{
    return requestToCurl(signer,
                         credentials,
                         request,
                         coreaws4::DateTimeUtils::now());
}

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        coreaws4::CredentialsProviderHandle credentialsProvider,
        const coreaws4::Request& request,
        std::time_t time)
{
    return requestToCurl((*signer)(credentialsProvider, request, time));
}


std::string requestToCurl(
        coreaws4::SignerHandle signer,
        coreaws4::CredentialsProviderHandle credentialsProvider,
        const coreaws4::Request& request)
{
    return requestToCurl(signer,
                         credentialsProvider,
                         request,
                         coreaws4::DateTimeUtils::now());
}

std::string requestToCurl(const coreaws4::Request& request)
{
    return httpRequestToCurl(coreaws4::HttpRequest(request));
}

std::string httpRequestToCurl(const coreaws4::HttpRequest& httpRequest)
{
    std::stringstream strbuf;

    strbuf << "curl -v -L \"" << httpRequest.getUrl() << '"';

    switch (httpRequest.getMethod())
    {
    case coreaws4::HttpMethod::GET:
        strbuf << " --get";
        break;
    case coreaws4::HttpMethod::DELETE:
        strbuf << " --request DELETE";
        break;
    case coreaws4::HttpMethod::HEAD:
        strbuf << " --head";
        break;
    case coreaws4::HttpMethod::PUT:
        strbuf << " --request PUT";
        strbuf << " --data-binary \"" << getInput(httpRequest) << '"';
        break;
    case coreaws4::HttpMethod::POST:
        strbuf << " --request POST";
        strbuf << " --data-binary \"" << getInput(httpRequest) << '"';
        break;
    }

    auto headers = httpRequest.getHeaders();
    auto ptr = headers.begin();
    auto end = headers.end();
    for (; ptr != end; ++ptr)
    {
        strbuf << " --header \"" << ptr->first;

        if (ptr->second != "")
        {
            strbuf << ": " << ptr->second;
        }
        else if (coreaws4::StringUtils::startsWithIgnoreCase(ptr->first, "x-"))
        {
            strbuf << ';';
        }
        else
        {
            strbuf << ':';
        }
        strbuf << '"';
    }

    return strbuf.str();
}

std::string getInput(const coreaws4::HttpRequest& httpRequest)
{
    coreaws4::InputStreamHandle inputStream = httpRequest.getContent();
    std::stringstream buf;
    while (inputStream->good())
    {
        char c = inputStream->get();
        if (inputStream->good())
        {
            if (c == '"')
            {
                buf << "\\\"";
            }
            else if (c == '\\')
            {
                buf << "\\\\";
            }
            else
            {
                buf << c;
            }
        }
    }

    return buf.str();
}

coreaws4::Credentials readCredentials()
{
    char* awsCredentialFile = getenv("AWS_CREDENTIAL_FILE");
    std::string path;
    if (NULL != awsCredentialFile)
    {
        path = awsCredentialFile;
    }
    else
    {
        path = "./aws_credentials";
    }

    std::ifstream in(path.c_str());
    if ((in.rdstate() & std::ifstream::failbit) != 0)
    {
        throw std::runtime_error("Cannot open credentials file");
    }

    std::string str;
    in >> str;
    int pos = str.find_first_of("=");
    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSAccessKeyId line, or no value");
    }

    std::string name = str.substr(0, pos);
    if (name != "AWSAccessKeyId")
    {
        throw std::runtime_error("Variable was not AWSAccessKeyId");
    }

    std::string awsAccessKeyId = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");

    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSSecretKey line, or no value");
    }

    name = str.substr(0, pos);
    if (name != "AWSSecretKey")
    {
        throw std::runtime_error("Variable was not AWSSecretKey");
    }

    std::string awsSecretKey = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");
    std::string sessionToken = "";
    if (pos != std::string::npos && pos != str.length() - 1)
    {
        name = str.substr(0, pos);
        if (name == "SessionToken")
        {
            sessionToken = str.substr(pos + 1);
        }
    }

    if (sessionToken != "")
    {
        return coreaws4::Credentials(
                awsAccessKeyId,
                awsSecretKey,
                sessionToken);
    }
    else
    {
        return coreaws4::Credentials(
                awsAccessKeyId,
                awsSecretKey);
    }

}

