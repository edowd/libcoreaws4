/*
 *
 * libcoreaws4/src/common.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__COMMON_HPP
#define COREAWS3__COMMON_HPP

#include <ctime>
#include <string>

#include "coreaws4/coreaws4.hpp"

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        coreaws4::CredentialsProviderHandle credentialsProvider,
        const coreaws4::Request& request,
        std::time_t time);

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        coreaws4::CredentialsProviderHandle credentialsProvider,
        const coreaws4::Request& request);

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        const coreaws4::Credentials& credentials,
        const coreaws4::Request& request,
        std::time_t time);

std::string requestToCurl(
        coreaws4::SignerHandle signer,
        const coreaws4::Credentials& credentials,
        const coreaws4::Request& request);

std::string requestToCurl(const coreaws4::Request& request);

std::string httpRequestToCurl(const coreaws4::HttpRequest& httpRequest);

std::string getInput(const coreaws4::HttpRequest& httpRequest);

coreaws4::Credentials readCredentials();

#endif // not COREAWS3__COMMON_HPP

