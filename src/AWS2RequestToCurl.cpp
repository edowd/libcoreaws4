/*
 *
 * libcoreaws4/src/AWS2RequestToCurl.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <string>

#include "common.hpp"

int main(int argc, char** argv)
{
    coreaws4::SignerHandle signer(new coreaws4::AWS2Signer());
    coreaws4::CredentialsProviderHandle credentialsProvider(
            new coreaws4::EnvironmentVariableCredentialsProvider());

    coreaws4::HttpMethod method = coreaws4::HttpMethod::POST;
    coreaws4::Endpoint endpoint(
            coreaws4::Scheme::https,
            "ec2.amazonaws.com",
            boost::optional<coreaws4::Port>());
    std::string resourcePath = "/";
    coreaws4::ParameterMap parameters;
    parameters.insert(coreaws4::ParameterEntry(
            "Version", 
            boost::optional<std::string>("2012-03-01")));
    parameters.insert(coreaws4::ParameterEntry(
            "Action",
            boost::optional<std::string>("DescribeAvailabilityZones")));
    coreaws4::HeaderMap headers;
    boost::optional<coreaws4::InputStreamHandle> content =
            boost::optional<coreaws4::InputStreamHandle>();

    auto request = coreaws4::Request(
            method,
            endpoint,
            resourcePath,
            parameters,
            headers,
            content);

    std::cout << requestToCurl(signer,
                               credentialsProvider,
                               request) << std::endl;

    return 0;
}

